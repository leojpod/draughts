(function (exports){
  console.log('draughts_ui loading...');
  var game_core;
  if(typeof(require) === 'undefined'){
    game_core = this.d_core;
  } else {
    game_core = require("/public/javascripts/draugths_core.js");
  }
  exports.Draughts_UI = function (game, div_board) {
    var ui = {}, pxSize, cellPxSize;
    ui.prototype = exports.Draughts_UI;
    ui.constructor = exports.Draughts_UI;
    function createEmptyBoard() {
      var x, y, size, div_line, a_cell;
      size = game.getSize();
      for (y = 0; y < size; y += 1) {
        div_line = jQuery('<div>').addClass('draughts_line').appendTo(div_board);
        for (x = 0; x < size; x += 1) {
          a_cell = jQuery('<div>').addClass('cell').appendTo(div_line);
          if ((x + y) % 2 === 0) {
            //white
            a_cell.addClass('white');
            // a_cell.data({hasPiece: false});
          }
          else {
            a_cell.addClass('black').data('has-piece',  false);
          }
          a_cell.attr({'data-x':x, 'data-y':y, 'data-pos': x + 'x' + y});
        }
      }
      jQuery('<canvas>').addClass('cellCanvas').appendTo(jQuery('.black'));
    }
    function drawPiece(canvas, size, piece){
      var ctx, radius;
      console.log('creating canvas!  -- ' + size);
      console.log('canvas size' + jQuery( canvas).width() + 'x' + jQuery( canvas).height());

      console.log('about to draw here: ' + piece.x() + 'x' + piece.y());
      console.log('conditions: ' + (piece instanceof game_core.Piece ) + ' && ' + (size > 0));
      // console.log('types : ' + piece.prototype + ' ' + typeof(game_core.Piece));
      // console.log(game_core.Piece);
      // console.log(piece.prototype);
      if (piece instanceof game_core.Piece && size > 0){
        ctx = canvas.getContext('2d');
        //let's clear the space first!
        // console.log('current tranformation ');
        // console.log(ctx.currentTransformation);
        // ctx.save();
        ctx.setTransform(1, 0, 0, 1, 0, 0);
        ctx.clearRect(0,0,size, size);
        // ctx.restore();
        //now let's draw!
        radius = size / 2;
        radius -= 2;
        console.log('radius ' + radius);
        if (piece.isKing()) {
          //draw a darker circle under the other
          ctx.beginPath();
          ctx.fillStyle = (piece.isWhite)? '#aaa' : '#000';
          ctx.arc(radius,radius + 4,radius,0,2*Math.PI);
          ctx.fill();
          ctx.beginPath();
          ctx.fillStyle= (piece.isWhite)? '#ddd' : '#444';
          ctx.arc(radius + 4,radius,radius,0,2*Math.PI);
          ctx.fill();
        } else {
          // ctx.beginPath();
          // ctx.fillStyle = (piece.isWhite)? '#aaa' : '#000';
          // ctx.arc(radius + 2,radius + 2,radius,0,2*Math.PI);
          console.log('drawing a man');
          ctx.fill();
          ctx.fillStyle='red';
          ctx.fillRect(0,0, size, size);
        }
      }
    }
    function drawIndication(canvas, size){
      var ctx = canvas.getContext('2d'),
        radius = (size/2) - 1;
      ctx.beginPath();
      ctx.lineWidth = 4;
      ctx.strokeStyle = '#2f2';
      ctx.arc(radius + 1, radius + 1, radius, 0, 2*Math.PI);
      ctx.stroke();
    }
    function redraw() {
      jQuery('.cell.black').each(function (idx, elm){
        var blackCell = jQuery(elm), data = blackCell.data();
        if (data.hasPiece === true){
          drawPiece(blackCell.children('canvas').get(0), cellPxSize, data.piece);
        }
      });
    }
    function init() {
      console.log('draugths ui init');
      var pieces, color, idx, pos, canvas, aPiece;
      //create the empty board,
      createEmptyBoard();
      //fill it up according to the game.board
      pieces = game.getBoard();
      console.log('pieces -->');
      console.log(pieces);
      for (color in pieces) {
        if (pieces.hasOwnProperty(color)) {
          for(idx = 0; idx < pieces[color].length; idx += 1) {
            aPiece = pieces[color][idx];
            pos = aPiece.x() + 'x' + aPiece.y();
            canvas = jQuery('.cell[data-pos="' + pos + '"]').data({hasPiece: true, piece: aPiece}).children('canvas').get(0);
            // drawPiece(canvas, cellPxSize, aPiece);
          }
        }
      }
      //place the ctrl to know what the user want to do...

      //place a listen on SizeChange for the div_board
      jQuery( div_board ).data('resize', function () {
      // jQuery( div_board ).resize( function () {
        // console.log('div-board resize');
        var height = jQuery(div_board).height(), width = jQuery(div_board).width();
        pxSize = (height > width) ? width : height;
        jQuery(div_board).height(pxSize).width(pxSize);
        cellPxSize = Math.floor((pxSize) / game.getSize());
        console.log('size: ' + pxSize + ' - ' + cellPxSize);
        jQuery('.draughts_line').height(cellPxSize);
        jQuery('.cell').height(cellPxSize).width(cellPxSize);
        jQuery('.cellCanvas').attr({width: cellPxSize, height: cellPxSize});
        redraw();
      });
      console.log('draugths ui init done');
    }
    init();
    // console.log('this versus exports.draugths_UI: ' + (this == exports.Draughts_UI) + '|' + (this === exports.Draughts_UI));
    // console.log(this);
    return ui;
  };
  console.log('draughts_ui loaded!');
}) (typeof exports === 'undefined'? this.d_ui={}: exports);