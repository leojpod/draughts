(function (exports){
  console.log('draughts_games loading...');
  var game_core, game_ui;
  if(typeof(require) === 'undefined'){
    game_core = this.d_core;
    game_ui = this.d_ui;
  } else {
    game_core = require("/public/javascripts/draugths_core.js");
    game_ui = require('/public/javascripts/draughts_ui.js');
  }

  exports.TestGame = function (div) {
    var core, ui;
    console.log('testgame about to be initialised');
    core = new game_core.Game(10, 'test');
    ui = new game_ui.Draughts_UI(core, div);
    console.log('testgame initialised without craps... ');
  };
  console.log('draughts_games loaded!');
}) (typeof exports === 'undefined'? this.d_games={}: exports);