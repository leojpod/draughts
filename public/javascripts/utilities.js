(function (exports) {
  exports.isInt = function (n) {
    return typeof n === 'number' && n % 1 === 0;
  };
}) (typeof exports === 'undefined'? this.utilities={}: exports);