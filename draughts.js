function Draughts_game() {
  var index, config, game;


  return {
    init : function (){
      index = jQuery("#landing_page");
      config = index.clone().appendTo(index.parent());
      game = index.clone().appendTo(index.parent());
      config.attr('id', 'config_page');
      game.attr('id', 'game_page');
      index.children().first().append(
        jQuery('<a>').attr({
          href: '#config_page'
        }).data({
          icon: 'gear',
          role: 'button',
          transition: 'slide',
          theme: 'a'
        }).text('Config').addClass('ui-btn-right')
        );
      config.children().first().children().first().text('Configuration');
      config.children().first().next().text('<p> There will be configuration there </p>');
      jQuery('div[data-role=page]').trigger('create');
    }
  }
}