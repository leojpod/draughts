 function start_game() {
  var game = new d_games.TestGame(jQuery('#board'));
  function resize_game() {
    var width = jQuery('body').width(), height = jQuery('body').height(), game_resize;
    console.log('window size:a ' + width + 'x' + height);
    width -= (jQuery('#board').outerWidth() -  jQuery('#board').innerWidth());
    height -= jQuery("div[data-role=header]").outerHeight();
    height -= jQuery("div[data-role=footer]").outerHeight();
    height -= (jQuery('#board').outerHeight() -  jQuery('#board').innerHeight());
    game_resize = jQuery('#board').height(height).width(width).data('resize'); // .resize()
    game_resize();
    // jQuery('#board').height(height).width(width);
  }

  var resizeTimer;
  jQuery(window).resize(function() {
    // console.log('window resize');
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(resize_game, 100);
  });
  jQuery( window ).resize();
}
jQuery( document ).delegate('#gamePage', 'pagecreate', function () {
  console.log('gamePage created!');
  start_game();
});