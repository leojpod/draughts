var make_draught_game = function (size, board_div) {
  var div_board = jQuery(board_div),
    div_line,
    board = [],
    //a_line = [],
    a_cell,
    state = {waiting: 0, moving: 1},
    action;

  function create_img_with_canvas(elm, data, size){
    var canvas, ctx, radius;
    // console.log('creating canvas! ' + data + ' -- ' + size);
    // console.log(elm);
    if (data.hasPiece === true && size > 0){
      canvas = jQuery(elm).children('canvas').get(0);
      ctx = canvas.getContext('2d');
      ctx.save();
      ctx.setTransform(1, 0, 0, 1, 0, 0);
      ctx.clearRect(0,0,size, size);
      ctx.restore();
      radius = size / 2;
      radius -= 2;
      if (data.isKing) {
        //draw a darker circle under the other
        ctx.beginPath();
        ctx.fillStyle = (data.isWhite)? '#aaa' : '#000';
        ctx.arc(radius,radius + 4,radius,0,2*Math.PI);
        ctx.fill();
        ctx.beginPath();
        ctx.fillStyle= (data.isWhite)? '#ddd' : '#444';
        ctx.arc(radius + 4,radius,radius,0,2*Math.PI);
        ctx.fill();
      } else {
        ctx.beginPath();
        ctx.fillStyle = (data.isWhite)? '#aaa' : '#000';
        ctx.arc(radius + 2,radius + 2,radius,0,2*Math.PI);
        ctx.fill();
      }
    }
  }
  function draw_indication(canvas, size){
    var ctx = canvas.getContext('2d'),
      radius = (size/2) - 1;
    ctx.beginPath();
    ctx.lineWidth = 4;
    ctx.strokeStyle = '#2f2';
    ctx.arc(radius + 1, radius + 1, radius, 0, 2*Math.PI);
    ctx.stroke();
  }
  function dropped(elm){
    console.log('dropped!');
    console.log(elm);
  }
  function init() {
    var x, y, data;
    for (y = 0; y < size; y += 1) {
      div_line = jQuery('<div>').addClass('draught_line').appendTo(div_board);
      board[y] = [];
      for (x = 0; x < size; x += 1) {
        a_cell = jQuery('<div>').addClass('cell').appendTo(div_line);
        if ((x + y) % 2 === 0) {
          //white
          a_cell.addClass('white');
          a_cell.data({hasPiece: false});
        }
        else {
          a_cell.addClass('black')
          if ( y < 2 ) {
            a_cell.data({hasPiece: true, isWhite: false, isKing: false}).addClass('taken');
            // data = a_cell.data();
            // console.log('adding a black man -> ' + data.hasPiece + ' / ' + data.isWhite + ' / ' + data.isKing);
            // console.log(a_cell);
          } else if ( y > 7) {
            a_cell.data({hasPiece: true, isWhite: true, isKing: false}).addClass('taken');
            // data = a_cell.data();
            // console.log('adding a white man -> ' + data.hasPiece + ' / ' + data.isWhite + ' / ' + data.isKing);
            // console.log(a_cell);
          } else {
            a_cell.data('has-piece',  false);
          }
        }
        board[y][x] = a_cell;
        a_cell.data({x:x, y:y});
      }
    }
    jQuery('<canvas>').addClass('cellCanvas').appendTo(jQuery('black'));
    //now put pictures in place
    redraw();
    //put the click listener in place
    // var selection = jQuery('.taken[data-isWhite="true"]').click(onClick);
    var selection = jQuery('.taken').each(function() {
      if (jQuery(this).data('isWhite')) {
        var elm = this;
        jQuery(this).click(function () {
          onClick(elm);
        });
      }
    });
    // console.log('added click listener on ' + selection.length+ ' elements');
    action = state.waiting;
  }
  function get_img_string(data, size){
    var img_name = '/public/images', alt_txt = '';
    if (data.hasPiece === true) {
      img_name += (data.isWhite)? 'white_' : 'black_';
      img_name += (data.isKing)? 'king' : 'man';
      // img_name += '_' + size;
      img_name += '.png';
      alt_txt += (data.isWhite)? 'white ' : 'black ';
      alt_txt += (data.isKing)? 'king' : 'man';
      return '<img src="'+img_name+'" alt="' + alt_txt + '"></img>';
    } else {
      return undefined;
    }
  }
  function onClick(elm){
    var elm_data;
    //locate the element
    elm_data = jQuery(elm).data();
    console.log('click on the man located at ' + elm_data.x + 'x' + elm_data.y);
    switch (action){
      case state.waiting:
        action = state.moving;
        pick_up(elm, elm_data);
        break;
      case state.moving:
        var isOver = dropped(elm, elm_data);
        if (isOver) {
          action = state.waiting;
        }
        break;
      default:
        console.log('this was not expected!');
    }
    // console.log(elm);
  }
  function pick_up(elm, elm_data) {
    console.log('pickup!');
    var candidates = [], cell, cell_data, i;
    if (typeof(elm_data) === 'undefined'){
      elm_data = jQuery(elm).data();
    }
    //look for where it could go 
    if (elm_data.isKing) {
      //then we will deal with that later.
    } else {
      //man can move forward in diagonal only
      if (elm_data.isWhite && elm_data.y > 0) {
        //test x+-1, y - 1
        if (elm_data.x > 0){
          cell = board[elm_data.y - 1][elm_data.x - 1];
          cell_data = cell.data();
          if (cell_data.hasPiece) {
            //then we should check color and free space after
          } else {
            candidates.push(cell);
          }
        }
        if (elm_data.x < (size - 1)) {
          cell = board[elm_data.y - 1][elm_data.x + 1];
          cell_data = cell.data();
          if (cell_data.hasPiece) {
            //then we should check color and free space after
          } else {
            candidates.push(cell);
          }
        }
      } else {
        //will deal with that later
      }
    }
    //draw indications on the candidates 
    for( i in candidates) {
      if(candidates.hasOwnProperty(i)){
        draw_indication(jQuery(candidates[i]).children('canvas').get(0), jQuery(candidates[i]).height());
      }
    }
    //draw indication on the current piece
    draw_indication(jQuery(elm).children('canvas').get(0), jQuery(elm).height());
  }


  function redraw() {
    console.log('redrawing!');
    jQuery(".cell").html('');
    jQuery(".taken").each(function () {
      create_img_with_canvas(this, jQuery(this).data(), jQuery(this).height());
    });
  }
  init();
  return {
    resize: function (available_height, available_width) {
      var max = (available_height < available_width)? available_height : available_width,
        cell_size = Math.floor((max) / size);
        // x, y, cell;
      console.log('[' + available_width + 'x' + available_height + '] max size is ' + max);
      console.log('cell_size : ' + cell_size);
      div_board.height(max).width(max);
      div_board.children('div').height(cell_size).children('div').height(cell_size).width(cell_size);
      redraw();
    }
  };
};