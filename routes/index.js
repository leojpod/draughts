
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', { title: 'Draughts HTML5', page_id: 'welcome' });
};
exports.draft_game = function(req, res){
  res.render('draft_game', { title: 'Draft game', size: '10', page_id: 'gamePage'});
};
exports.test_game = function(req, res){
  res.render('test_game', { title: 'Test game', size: '10', page_id: 'gamePage'});
};