(function (exports) {
  console.log('draugths_core loading... ');
  var utils;
  if(typeof(require) === 'undefined'){
    utils = this.utilities;
  } else {
    utils = require("/public/javascripts/utilities.js");
  }
  exports.Piece = function(x, y, isWhite, isKing) {
    var piece = this, _x = x, _y = y, _isWhite = isWhite, _isKing = isKing;
    // piece.prototype = exports.Piece;
    // piece.constructor = exports.Piece;
    // piece.prototype = this;
    // piece.constructor = this;
    if (typeof(x) === 'undefined' ||
        typeof(y) === 'undefined' ||
        typeof(isWhite) === 'undefined' ){
      throw "missing parameter";
    }

    piece.getCoordinates = function () {
      return {x: _x, y: _y};
    };
    piece.getColor = function () {
      return (_isWhite)? 'white': 'black';
    };
    piece.isWhite = function () {
      return _isWhite;
    };
    piece.isKing = function () {
      return _isKing;
    };
    piece.x = function (val) {
      if (typeof(val) === 'undefined') {
        return _x;
      } else if (utils.isInt(val)) {
        _x = val;
      }
    };
    piece.y = function (val) {
      if (typeof(val) === 'undefined') {
        return _y;
      } else if (utils.isInt(val)) {
        _y = val;
      }
    };
    // return piece;
  };

  exports.Board = function (size){
    var board = this, _pieces = {}, _size;
    // board.prototype = exports.Board;
    // board.constructor = exports.Board;
    if (typeof(size) === 'undefined') {
      throw 'you need to give a size for the board!';
    }
    _size = size;
    board.getPieces = function (color) {
      if (typeof(color) === 'undefined') {
        return _pieces;
      } else {
        if (_pieces.hasOwnProperty(color)) {
          return _pieces[color];
        } else {
          throw 'unknown color ' + color;
        }
      }
    };
    board.addPiece = function (aPiece) {
      if (! aPiece instanceof exports.Piece) {
        throw 'wrong argument, ' + aPiece + ' is not a valid piece';
      } //else
      if (board.isIn(aPiece.x(), aPiece.y()) && board.isFree(aPiece.x(), aPiece.y())){
        if ( ! _pieces.hasOwnProperty(aPiece.getColor()) ){
          _pieces[aPiece.getColor()] = [];
        }
        _pieces[aPiece.getColor()].push(aPiece);
      } else {
        throw "the board is not free!";
      }
    };
    board.addPieces = function (manyPieces) {
      var aPiece, idx;
      if (! jQuery.isArray(manyPieces)) {
        throw 'wrong argument... this is not even an array';
      }
      for (idx = 0; idx < manyPieces.length ; idx +=1) {
        aPiece = manyPieces[idx];
        console.log(manyPieces);
        console.log(aPiece);
        if (! aPiece instanceof exports.Piece
            || ! board.isIn(aPiece.x(), aPiece.y())
            || ! board.isFree(aPiece.x(), aPiece.y())) {
          console.log( 'isIn ? ' + board.isIn(aPiece.x(), aPiece.y()));
          console.log( 'isFree ? ' + board.isFree(aPiece.x(), aPiece.y()));
          console.log( 'coord: ' + typeof aPiece.x() + 'x' + typeof aPiece.y() + '  \t  ' + aPiece.x() + 'x' + aPiece.y());
          console.log( 'isInt ? ' + utils.isInt(aPiece.x()));
          console.log( 'n % 1 : ' + aPiece.x() % 1 + ' \t' + 5 % 1 + '\t' + 5.2 % 1);
          throw 'at least one element of the parameter does not match all the requirements';
        }
      }
      for (idx = 0; idx < manyPieces.length ; idx +=1) {
        aPiece = manyPieces[idx];
        board.addPiece(aPiece);
      }
    };
    board.isFree = function (x,y) {
      var color, aPiece, idx;
      for (color in _pieces) {
        if (_pieces.hasOwnProperty(color)) {
          //then let's check every piece!
          for (idx = 0; idx < _pieces[color].length; idx += 1) {
            aPiece = _pieces[color][idx];
            if (x == aPiece.x() && y == aPiece.y()) {
              return false;
            }
          }
        }
      }
      // if nothing has been found so far then we can assume it is free..
      return true;
    };
    board.isIn = function (x, y) {
      return utils.isInt(x) && utils.isInt(y) && x >= 0 && y >= 0 && x < _size && y < _size;
    };
    board.contains = function (piece) {
      var anotherPiece;
      if (! piece instanceof exports.Piece) {
        throw 'the parameter is not correct: not a Piece!';
      }
      anotherPiece = board.getPieceAt(piece.x(),piece.y(), piece.getColor());
      if (anotherPiece === null) {
        return false;
      }
      if (anotherPiece.isKing() !== piece.isKing()) {
        return false;
      }
      return true;
    };
    board.getPieceAt = function (x, y, color) {
      var aPiece;
      function checkAColor (color, x, y) {
        var idx, aPiece;
        for (idx = 0; idx < _pieces[color].length; idx += 1) {
          aPiece = _pieces[color][idx];
          if (aPiece.x() === x && aPiece.y() === y) {
            return aPiece;
          }
        }
        return null;
      }
      if (typeof color !== undefined){
        if (_pieces.hasOwnProperty(color)) {
          return checkAColor(color, x, y);
        } else {
          return null;
        }
      } // else
      for (color in _pieces) {
        if (_pieces.hasOwnProperty(color)) {
          aPiece = checkAColor(color, x, y);
          if (aPiece !== null) {
            return aPiece;
          } //else go on looking
        }
      }
      //if nothing as been found so far there is nothing to find...
      return null;
    };
    board.getSize = function () {
      return _size;
    };

    // return board;
  };

  exports.Rules = function () {
    var rules = this;
    // rules.prototype = exports.Rules;
    // rules.constructor = exports.Rules;
    rules.init = function (board, size) {
      throw 'unimplemented!';
    };
    rules.isLegalMove = function (board, piece, newX, newY) {
      throw 'unimplemented!';
    };
    rules.getLegalMoves = function (board, piece) {
      throw 'unimplemented!';
    };
    rules.isWhiteTurn = function () {
      throw 'unimplemented!';
    };
    // return rules;
  };

  exports.StdRules = function () {
    var rules = new exports.Rules();
    // this.prototype = exports.Rules;
    // rules.prototype = exports.StdRules;
    // rules.constructor = exports.StdRules;
    rules.init = function (board, size) {
      var pieces = [], x, y;
      //add the black pieces;
      for (y = 0; y < 2; y += 1){
        for (x = 0; x < size; x += 1){
          if ( (x + y) % 2 !== 0) {
            pieces.push(new exports.Piece(x, y, false, false));
          }
        }
      }
      //now add the white ones...
      for (y = size -1; y > size - 3; y -=1) {
        for (x = 0; x < size; x += 1) {
          if ( (x + y) % 2 !== 0) {
            pieces.push(new exports.Piece(x, y, true, false));
          }
        }
      }
      board.addPieces(pieces);
    };
    // return rules;
  };
  exports.StdRules.prototype = new exports.Rules();
  exports.StdRules.prototype.constructor = exports.StdRules;

  exports.TestRules = function () {
    var rules = this;
    // this.prototype = new exports.Rules();
    // rules.prototype = exports.TestRules;
    // rules.constructor = exports.TestRules;
    rules.init = function (board, size) {
      var theSinglePiece, x = Math.floor(size / 2), y = x + 1;
      // console.log('test piece at ' + x + 'x' + y);
      theSinglePiece = new exports.Piece(x, y, true, false);
      board.addPieces(new Array(theSinglePiece));
    };
    rules.isLegalMove = function (board, piece, newX, newY) {
      if (newX >= 0 && newX < board.getSize()) {
        if (newY >= 0 && newY < board.getSize()) {
          if ( (newX + newY) % 2 !== 0) {
            return true;
          }
        }
      }
      return false;
    };
    rules.getLegalMoves = function (board, piece) {
      var moves = [];
      if (! board.contains(piece)) {
        return null;
      }
      // else
      if (piece.isWhite()) {
        if (board.isFree(piece.x() - 1, piece.y() - 1)){
          moves.push({x: piece.x() - 1, y: piece.y() - 1});
        }
        if (board.isFree(piece.x() + 1, piece.y() - 1)){
          moves.push({x: piece.x() - 1, y: piece.y() - 1});
        }
        return moves;
      }
      return null;
    };
    // return rules;
  };
  exports.TestRules.prototype = new exports.Rules();
  exports.TestRules.prototype.constructor = exports.TestRules;

  exports.Game = function (size, rules){
    var game = {}, _board, _rules;
    // game.prototype = exports.Game;
    // game.constructor = exports.Game;
    if (typeof(rules) === 'undefined') {
      rules = 'std';
    }
    if (typeof(size) === 'undefined') {
      size = 10;
    }
    //perform the initialisation
    function init(){
      switch(rules) {
        case 'std':
          _rules = new exports.StdRules();
          break;
        case 'test':
          _rules = new exports.TestRules();
          break;
        default:
          throw 'unsupported rules';
      }
      _board = new exports.Board(size);
      console.log('rules? ' + (_rules instanceof exports.Rules) + ' | ' + (_rules instanceof exports.TestRules));
      _rules.init(_board, size);
    }
    init();

    //create the interface:
    game.getBoard = function (){
      return _board.getPieces();
    };
    game.getSize = function () {
      return size;
    };
    return game;
  };

  // exports.isPiece = function (obj) {
  //   return obj instanceof exports.Piece;
  // };

  // console.log(new exports.Piece(0,0,false, false) instanceof exports.Piece);
  console.log('draughts_core loaded');
}) (typeof exports === 'undefined'? this.d_core={}: exports);